-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 160.10.25.16:3306
-- Generation Time: Dec 03, 2018 at 01:40 PM
-- Server version: 5.7.24-log
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cs3230f18g`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `adminId` int(11) NOT NULL,
  `id` varchar(20) CHARACTER SET latin1 NOT NULL,
  `firstname` varchar(20) NOT NULL,
  `lastname` varchar(20) NOT NULL,
  `ssn` char(9) NOT NULL,
  `dob` date NOT NULL,
  `sex` char(1) NOT NULL,
  `address` varchar(20) NOT NULL,
  `phone` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`adminId`, `id`, `firstname`, `lastname`, `ssn`, `dob`, `sex`, `address`, `phone`) VALUES
(1, 'a00111', 'Joel', 'Patterson', '222551111', '1988-11-10', 'M', '444 4th st', '7709992222');

-- --------------------------------------------------------

--
-- Table structure for table `appointment`
--

CREATE TABLE `appointment` (
  `aId` int(11) NOT NULL,
  `pId` int(11) NOT NULL,
  `dId` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `description` varchar(200) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `appointment`
--

INSERT INTO `appointment` (`aId`, `pId`, `dId`, `date`, `description`) VALUES
(13, 12, 3, '2018-11-12 13:00:00', 'edit testing'),
(14, 1, 1, '2018-11-18 09:30:00', 'Broken arm'),
(15, 3, 1, '2018-11-18 09:00:00', 'Strep Throat'),
(16, 19, 1, '2018-11-18 11:00:00', 'Cold'),
(17, 3, 3, '2019-08-15 11:00:00', 'Neck Pains'),
(18, 14, 3, '2018-11-19 03:11:00', 'Stomach pain'),
(19, 15, 1, '2018-11-19 08:13:00', 'Chest Pains'),
(20, 2, 3, '2018-12-25 08:05:00', 'Neck Hurts'),
(21, 12, 3, '2018-11-26 13:10:00', 'Physical'),
(22, 12, 3, '2018-11-28 10:49:00', 'Sore Throat'),
(23, 22, 3, '2018-11-28 15:19:00', 'jjj'),
(25, 2, 3, '2018-12-01 00:41:00', 'testing add');

-- --------------------------------------------------------

--
-- Table structure for table `complete_appointments`
--

CREATE TABLE `complete_appointments` (
  `aId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `complete_appointments`
--

INSERT INTO `complete_appointments` (`aId`) VALUES
(13),
(14),
(16),
(18),
(19),
(20),
(23);

-- --------------------------------------------------------

--
-- Table structure for table `doctor`
--

CREATE TABLE `doctor` (
  `doctorId` int(11) NOT NULL,
  `id` varchar(20) CHARACTER SET latin1 NOT NULL,
  `firstname` varchar(20) NOT NULL,
  `lastname` varchar(20) NOT NULL,
  `ssn` char(9) NOT NULL,
  `dob` date NOT NULL,
  `sex` char(1) NOT NULL,
  `address` varchar(20) NOT NULL,
  `phone` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `doctor`
--

INSERT INTO `doctor` (`doctorId`, `id`, `firstname`, `lastname`, `ssn`, `dob`, `sex`, `address`, `phone`) VALUES
(1, 'd00111', 'Robert', 'Frost', '123456789', '1984-10-09', 'F', '123 maple st', '6787779999'),
(3, 'd00112', 'Angela', 'Stone', '987654321', '1974-10-09', 'F', '123 long st', '6787779999');

-- --------------------------------------------------------

--
-- Table structure for table `incomplete_appointments`
--

CREATE TABLE `incomplete_appointments` (
  `aId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `incomplete_appointments`
--

INSERT INTO `incomplete_appointments` (`aId`) VALUES
(15),
(17),
(21),
(22),
(25);

-- --------------------------------------------------------

--
-- Table structure for table `lab_test`
--

CREATE TABLE `lab_test` (
  `code` varchar(20) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lab_test`
--

INSERT INTO `lab_test` (`code`, `name`) VALUES
('ANA', 'Antinuclear Antibody'),
('BMP', 'Basic Metabolic Panel'),
('CBC', 'Complete Blood Count'),
('CMP', 'Comprehensive Metabolic Panel'),
('ESR', 'Sedimentation Rate'),
('Flu', 'Influenza A and B Screen'),
('LFT', 'Liver Function Panel'),
('PSA', 'Prostate Specific Antigen'),
('PTT', 'Partial Thromboplastin Time'),
('TSH', 'Thyroid Stimulating Hormone');

-- --------------------------------------------------------

--
-- Table structure for table `logins`
--

CREATE TABLE `logins` (
  `id` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logins`
--

INSERT INTO `logins` (`id`, `password`) VALUES
('a00111', 'ôy–Ë{Ð4\0q2õÃÚ`v'),
('d00111', 'ôy–Ë{Ð4\0q2õÃÚ`v'),
('d00112', 'ôy–Ë{Ð4\0q2õÃÚ`v'),
('n00111', 'ôy–Ë{Ð4\0q2õÃÚ`v'),
('n00112', 'µ.ñÛ¿—hz‡²›u]ß‚'),
('n00117', 'µ.ñÛ¿—hz‡²›u]ß‚'),
('n00122', 'µ.ñÛ¿—hz‡²›u]ß‚'),
('n00123', '12345');

-- --------------------------------------------------------

--
-- Table structure for table `nurse`
--

CREATE TABLE `nurse` (
  `nurseId` int(11) NOT NULL,
  `userId` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `nurse`
--

INSERT INTO `nurse` (`nurseId`, `userId`) VALUES
(1, 'n00111'),
(2, 'n00117'),
(3, 'n00122'),
(4, 'n00123');

-- --------------------------------------------------------

--
-- Table structure for table `ordered_test`
--

CREATE TABLE `ordered_test` (
  `id` int(11) NOT NULL,
  `patientID` int(11) NOT NULL,
  `order_date` date NOT NULL,
  `due_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ordered_test`
--

INSERT INTO `ordered_test` (`id`, `patientID`, `order_date`, `due_date`) VALUES
(23, 1, '2018-11-28', '2018-12-05'),
(24, 1, '2018-11-28', '2018-12-05'),
(25, 22, '2018-11-28', '2018-12-05'),
(27, 22, '2018-12-03', '2018-12-10'),
(28, 22, '2018-12-03', '2018-12-10');

-- --------------------------------------------------------

--
-- Table structure for table `ordered_test_components`
--

CREATE TABLE `ordered_test_components` (
  `orderID` int(11) NOT NULL,
  `code` varchar(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `result` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ordered_test_components`
--

INSERT INTO `ordered_test_components` (`orderID`, `code`, `name`, `result`) VALUES
(23, 'ANA', 'Antinuclear Antibody', '13'),
(23, 'CBC', 'Complete Blood Count', '50'),
(23, 'ESR', 'Sedimentation Rate', '50'),
(23, 'LFT', 'Liver Function Panel', '50'),
(23, 'PTT', 'Partial Thromboplastin Time', '50'),
(24, 'ANA', 'Antinuclear Antibody', '15'),
(24, 'CBC', 'Complete Blood Count', '16'),
(25, 'ANA', 'Antinuclear Antibody', '18'),
(25, 'CBC', 'Complete Blood Count', '18'),
(25, 'ESR', 'Sedimentation Rate', '19'),
(28, 'ANA', 'Antinuclear Antibody', 'Pending'),
(28, 'CBC', 'Complete Blood Count', 'Pending');

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

CREATE TABLE `patient` (
  `id` int(11) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `ssn` char(9) NOT NULL,
  `dob` date NOT NULL,
  `sex` varchar(20) NOT NULL,
  `address` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state` char(2) NOT NULL,
  `zip` int(11) NOT NULL,
  `phone` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `patient`
--

INSERT INTO `patient` (`id`, `fname`, `lname`, `ssn`, `dob`, `sex`, `address`, `city`, `state`, `zip`, `phone`) VALUES
(1, 'Nnenna', 'Aneke', '123456789', '1999-11-11', 'Female', '123 Maple St', 'Carrollton', 'GA', 30118, '1111111111'),
(2, 'Amanda', 'Lewis', '987654321', '1997-11-11', 'Female', '848 Atlanta St', 'Atlanta', 'GA', 30995, '2233322222'),
(3, 'Phillip', 'Morris', '789654123', '2018-10-29', 'Male', 'gfsgsggf', 'Atlanta', 'GA', 12345, '5555555555'),
(12, 'Justin', 'Allen', '333333333', '2000-01-01', 'm', '123 t st', 'd', 'ga', 33333, '123456789'),
(13, 'Amber', 'Brayboy', '444444444', '1993-03-29', 'F', '2580 Waterfall st', 'Ellenwood', 'GA', 30294, '987654321'),
(14, 'Daniel', 'Stone', '222990000', '2018-11-17', 'Male', '123 my rd', 'Decatur', 'GA', 30294, '5553437777'),
(15, 'Kamille', 'Rogers', '333335555', '2018-11-17', 'Female', '123 my ln', 'Atlanta', 'GA', 22221, '8886665555'),
(16, 'David', 'Brayboy', '333551111', '1971-11-17', 'Male', '123 h rd', 'Ellenwood', 'GA', 30294, '7707771111'),
(17, 'Nathan', 'Jones', '111111111', '1995-11-17', 'Male', 'ddd th rd', 'ATL', 'AL', 22222, '1112223333'),
(19, 'Carmen', 'Jenkins', '111223333', '1994-11-17', 'Female', '4999 Long st', 'Bowdon', 'GA', 30038, '8082229900'),
(20, 'Jasmine', 'Smith', '222335555', '1990-11-18', 'Female', '3221 Long dr', 'Carrollton', 'GA', 30117, '7709999999'),
(21, 'Joe', 'Rogers', '555661111', '1999-12-12', 'Male', '123 My House', 'Carrollton', 'GA', 30117, '7709998888'),
(22, 'Nnenna', 'Aneke', '989665444', '2018-11-28', 'Female', 'fgsrf', 'ergerg', 'AL', 22222, '9999999999');

-- --------------------------------------------------------

--
-- Table structure for table `routine_check`
--

CREATE TABLE `routine_check` (
  `aId` int(11) NOT NULL,
  `nId` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `systolicReading` double NOT NULL,
  `diastolicBloodPressure` double NOT NULL,
  `weight` double NOT NULL,
  `temp` double NOT NULL,
  `pId` int(11) NOT NULL,
  `init_diagnosis` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `routine_check`
--

INSERT INTO `routine_check` (`aId`, `nId`, `date`, `systolicReading`, `diastolicBloodPressure`, `weight`, `temp`, `pId`, `init_diagnosis`) VALUES
(19, 1, '2018-11-28 10:48:00', 55, 66, 105, 0, 15, 'Light Cold'),
(20, 1, '2018-09-19 20:08:00', 4, 6, 6, 0, 2, 'Testing final'),
(23, 1, '2018-11-28 15:22:00', 99, 77, 1, 0, 22, 'Test on patients page');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` varchar(20) NOT NULL,
  `firstname` varchar(20) NOT NULL,
  `lastname` varchar(20) NOT NULL,
  `ssn` char(9) NOT NULL,
  `dob` date NOT NULL,
  `sex` char(1) NOT NULL,
  `address` varchar(20) NOT NULL,
  `phone` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `firstname`, `lastname`, `ssn`, `dob`, `sex`, `address`, `phone`) VALUES
('n00111', 'Jane', 'Doe', '111225555', '1981-08-07', 'F', '555 Cross St', '7705554444'),
('n00117', 'Stacy', 'Lewis', '838221111', '1994-12-03', 'F', '123 the st', '7707707700'),
('n00122', 'Janet', 'Jones', '222334488', '2018-12-03', 'F', '333 rd', '7708088080'),
('n00123', 'John', 'Green', '665331144', '1999-12-03', 'M', 'rrtt', '7705554444'),
('n005551', 'Carrie', 'Lewis', '333221145', '2000-12-03', 'F', 'ffdf', '7708089999'),
('n005555', 'Carrie', 'Lewis', '333221147', '2000-12-03', 'F', 'ffdf', '7708089999');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`adminId`),
  ADD UNIQUE KEY `ssn` (`ssn`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `appointment`
--
ALTER TABLE `appointment`
  ADD PRIMARY KEY (`aId`),
  ADD UNIQUE KEY `dId` (`dId`,`date`),
  ADD KEY `pId` (`pId`);

--
-- Indexes for table `complete_appointments`
--
ALTER TABLE `complete_appointments`
  ADD PRIMARY KEY (`aId`);

--
-- Indexes for table `doctor`
--
ALTER TABLE `doctor`
  ADD PRIMARY KEY (`doctorId`),
  ADD UNIQUE KEY `ssn` (`ssn`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `incomplete_appointments`
--
ALTER TABLE `incomplete_appointments`
  ADD PRIMARY KEY (`aId`);

--
-- Indexes for table `lab_test`
--
ALTER TABLE `lab_test`
  ADD PRIMARY KEY (`code`,`name`);

--
-- Indexes for table `logins`
--
ALTER TABLE `logins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nurse`
--
ALTER TABLE `nurse`
  ADD PRIMARY KEY (`nurseId`),
  ADD KEY `userId` (`userId`);

--
-- Indexes for table `ordered_test`
--
ALTER TABLE `ordered_test`
  ADD PRIMARY KEY (`id`),
  ADD KEY `patientID` (`patientID`);

--
-- Indexes for table `ordered_test_components`
--
ALTER TABLE `ordered_test_components`
  ADD PRIMARY KEY (`orderID`,`code`,`name`),
  ADD KEY `code` (`code`,`name`);

--
-- Indexes for table `patient`
--
ALTER TABLE `patient`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `ssn` (`ssn`);

--
-- Indexes for table `routine_check`
--
ALTER TABLE `routine_check`
  ADD PRIMARY KEY (`aId`),
  ADD KEY `nId` (`nId`),
  ADD KEY `pId` (`pId`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ssn` (`ssn`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `adminId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `appointment`
--
ALTER TABLE `appointment`
  MODIFY `aId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `doctor`
--
ALTER TABLE `doctor`
  MODIFY `doctorId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `nurse`
--
ALTER TABLE `nurse`
  MODIFY `nurseId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ordered_test`
--
ALTER TABLE `ordered_test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `patient`
--
ALTER TABLE `patient`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin`
--
ALTER TABLE `admin`
  ADD CONSTRAINT `admin_ibfk_1` FOREIGN KEY (`id`) REFERENCES `logins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `appointment`
--
ALTER TABLE `appointment`
  ADD CONSTRAINT `appointment_ibfk_1` FOREIGN KEY (`pId`) REFERENCES `patient` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `complete_appointments`
--
ALTER TABLE `complete_appointments`
  ADD CONSTRAINT `complete_appointments_ibfk_1` FOREIGN KEY (`aId`) REFERENCES `appointment` (`aId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `doctor`
--
ALTER TABLE `doctor`
  ADD CONSTRAINT `doctor_ibfk_1` FOREIGN KEY (`id`) REFERENCES `logins` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `incomplete_appointments`
--
ALTER TABLE `incomplete_appointments`
  ADD CONSTRAINT `incomplete_appointments_ibfk_1` FOREIGN KEY (`aId`) REFERENCES `appointment` (`aId`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `nurse`
--
ALTER TABLE `nurse`
  ADD CONSTRAINT `nurse_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ordered_test`
--
ALTER TABLE `ordered_test`
  ADD CONSTRAINT `ordered_test_ibfk_1` FOREIGN KEY (`patientID`) REFERENCES `patient` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `ordered_test_components`
--
ALTER TABLE `ordered_test_components`
  ADD CONSTRAINT `ordered_test_components_ibfk_1` FOREIGN KEY (`orderID`) REFERENCES `ordered_test` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `ordered_test_components_ibfk_2` FOREIGN KEY (`code`,`name`) REFERENCES `lab_test` (`code`, `name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `routine_check`
--
ALTER TABLE `routine_check`
  ADD CONSTRAINT `routine_check_ibfk_1` FOREIGN KEY (`nId`) REFERENCES `nurse` (`nurseId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `routine_check_ibfk_2` FOREIGN KEY (`aId`) REFERENCES `appointment` (`aId`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `routine_check_ibfk_3` FOREIGN KEY (`pId`) REFERENCES `patient` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
